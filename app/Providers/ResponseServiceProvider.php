<?php

namespace App\Providers;

use Illuminate\Routing\ResponseFactory;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use Throwable;

class ResponseServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @param ResponseFactory $factory
     * @return void
     */
    public function boot(ResponseFactory $factory)
    {

        try {
            $getSuccessFormat = function (array $data, array $notifications = [], $async = false) {
                return $this->getSuccessFormat($data, $notifications, $async);
            };
            $getErrorFormat = function (Throwable $e, $async = false) {
                return $this->getErrorFormat($e, $async);
            };


            $factory->macro(
                'sendSuccess',
                function (
                    array $data = [],
                    int $statusCode = 200,
                    array $notifications = [],
                    $async = false
                ) use (
                    $factory,
                    $getSuccessFormat
                ) {

                    $format = $getSuccessFormat($data, $notifications, $async);
                    return $factory->make($format, $statusCode);
                }
            );

            $factory->macro(
                'sendError',
                function (
                    Throwable $e,
                    int $statusCode = 500,
                    $async = false
                ) use (
                    $factory,
                    $getErrorFormat
                ) {

                    $format = $getErrorFormat($e, $async);
                    return $factory->make($format, $statusCode);
                }
            );
        } catch (Throwable $e) {
            $format = $this->getErrorFormat($e);
            return $factory->make($format, 500);
        }
    }

    protected function getSuccessFormat(array $data, array $notifications = [], $async = false): array
    {
        $meta = [
            'success' => true,
            'async' => $async,
        ];

        return [
            'data' => $data,
            'notifications' => $notifications,
            'meta' => $meta
        ];
    }

    protected function getErrorFormat(Throwable $e, $async = false): array
    {
        if ($e instanceof ValidationException) {
            $errors = $this->getValidationErrors($e);
        } else {
            $errors = $this->getCommonErrors($e);
        }

        return [
            'errors' => $errors,
            'meta' => [
                'success' => false,
                'async' => $async
            ]
        ];
    }

    protected function getValidationErrors(ValidationException $e): array
    {
        $errors = [];
        $alias = Str::snake(class_basename($e));
        foreach ($e->errors() as $fieldName => $fieldErrors) {
            foreach ($fieldErrors as $fieldError) {
                $error = [
                    'alias' => $alias,
                    'message' => $fieldError,
                    'params' => (object) [
                        'field_name' => $fieldName,
                    ],
                ];
                $errors[] = $error;
            }
        }
        return $errors;
    }

    protected function getCommonErrors(Throwable $e): array
    {
        $alias = Str::snake(class_basename($e));
        $messageAlias = "errors.$alias.message";
        $message = __("errors.$alias.message");
        if ($message == $messageAlias) {
            $message = __('errors.default.message');
        }
        $error = [
            'alias' => $alias,
            'message' => $message,
            'params' => (object) [],
        ];


        if (env('APP_DEBUG')) {
            $error['details'] = $e->getMessage();
        }
        return [$error];
    }
}
