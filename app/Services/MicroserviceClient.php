<?php

namespace App\Services;

use App\Http\Requests\GatewayRequest;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Client\ConnectionException;
use Illuminate\Http\Client\PendingRequest;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;

class MicroserviceClient
{
    /**
     * Для любого запроса проверяет авторизацию в микросервисе auth
     * @param Request $request
     * @return bool
     */
    public function verify(Request $request)
    {
        $configPath = 'microservices.auth';
        $url = $this->getUrl($configPath, 'verify');
        $headers = [];
        $authHeader = $request->header('authorization');
        if (empty($authHeader)) {
            //Пользователь не авторизован
            return false;
        }
        $headers['Authorization'] = $authHeader;
        /** @var Response $response */
        $response = \Http::withHeaders($headers)->get($url);
        return $response->getStatusCode() === 200;
    }

    /**
     * @param GatewayRequest $request
     * @param string $url URL метода микросервиса, который нужно вызвать
     */
    public function gatewayRequest(GatewayRequest $request, $url)
    {
        $requestContentType = $request->getFullContentType();
        $queryString = $request->getQueryString();
        $url = empty($queryString) ? $url : $url . '?' . $queryString;
        /** @var Response $response */
        $method = $request->getMethod();
        $headers = $request->getGatewayAllowedHeaders();
        $timeout = config('microservices.timeout');
        switch ($method) {
            case 'GET':
                $response = \Http::timeout($timeout)->withHeaders($headers)->get($url);
                break;
            case 'DELETE':
                $response = \Http::timeout($timeout)->withHeaders($headers)->delete($url);
                break;
            case 'POST':
                $content = $request->getContent();
                /** @var PendingRequest $httpRequest */
                $httpRequest = \Http::timeout($timeout)->withHeaders($headers);
                if ($request->files->count()) {
                    $this->attachFilesFromRequest($request, $httpRequest);
                } else {
                    $httpRequest->withBody($content, $requestContentType);
                }

                $response = $httpRequest->post($url);
                break;
            case 'PUT':
                $content = $request->getContent();
                $response = \Http::timeout($timeout)->withHeaders($headers)
                    ->withBody($content, $requestContentType)
                    ->put($url);
                break;
            default:
                throw new \Exception('Unsupported method');
        }
        if ($response->getStatusCode() == 502) {
            throw new ConnectionException('Bad gateway');
        }

        return response($response, $response->getStatusCode())
            ->header('Content-Type', $response->getHeader('Content-Type'));
    }

    public function getUrl(string $configPath, string $alias, array $urlParams = []): string
    {
        $url = config($configPath . '.' . $alias);
        return sprintf($url, ...$urlParams);
    }

    protected function attachFilesFromRequest(GatewayRequest $gatewayRequest, PendingRequest $httpRequest)
    {
        /**
         * @var string $fileName
         * @var UploadedFile $file
         */
        foreach ($gatewayRequest->files->all() as $fileName => $file) {
            $httpRequest->attach($fileName, $file->getContent(), $file->getClientOriginalName());
        }
    }

}
