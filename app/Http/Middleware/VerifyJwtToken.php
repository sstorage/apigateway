<?php

namespace App\Http\Middleware;

use App\Services\MicroserviceClient;
use Closure;
use Illuminate\Auth\Middleware\Authenticate as Middleware;

class VerifyJwtToken extends Middleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string[]  ...$guards
     * @return mixed
     *
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function handle($request, Closure $next, ...$guards)
    {
        /** @var MicroserviceClient $client */
        $client = app()->make(MicroserviceClient::class);
        if (!$client->verify($request)) {
            //Если пользователь не авторизован, то убираем его токен и делаем запрос без него
            $request->headers->remove('authenticate');
        }

        return $next($request);
    }
}
