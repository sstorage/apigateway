<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class GatewayRequest extends FormRequest
{
    public function getFullContentType(): ?string
    {
        return $this->header('Content-Type', 'application/json');
    }

    public function getMethod()
    {
        return strtoupper($_SERVER['REQUEST_METHOD'] ?? 'GET');
    }

    public function getHeaders(): array
    {
        $headers = $this->header();
        if (isset($headers['host'])) {
            unset($headers['host']);
        }
        return $headers;
    }

    public function getGatewayAllowedHeaders(): array
    {
        $requestHeaders = $this->header();
        $allowed = [
            'authorization',
            'user-agent',
            'accept',
            'cache-control',
            'accept-encoding',
        ];
        foreach ($requestHeaders as $headerName => $header) {
            if (!in_array($headerName, $allowed)) {
                unset($requestHeaders[$headerName]);
            }
        }

        return $requestHeaders;
    }

    public function rules()
    {
        return [];
    }
}
