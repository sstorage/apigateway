<?php

namespace App\Http\Controllers;

use App\Http\Requests\GatewayRequest;

class DownloaderController extends GatewayBaseController
{
    public function getFile(GatewayRequest $request, ...$params)
    {
        return $this->handle($request,'file', $params);
    }

    protected function getConfigPath(): string
    {
        return 'microservices.downloader';
    }
}
