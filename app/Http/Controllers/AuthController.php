<?php

namespace App\Http\Controllers;

use App\Http\Requests\GatewayRequest;

class AuthController extends GatewayBaseController
{
    public function login(GatewayRequest $request)
    {
        return $this->handle($request, 'login');
    }

    public function registration(GatewayRequest $request)
    {
        return $this->handle($request, 'registration');
    }

    public function logout(GatewayRequest $request)
    {
        return $this->handle($request, 'logout');
    }

    public function refresh(GatewayRequest $request)
    {
        return $this->handle($request, 'refresh');
    }

    public function userInfo(GatewayRequest $request)
    {
        return $this->handle($request, 'user');
    }

    public function update(GatewayRequest $request)
    {
        return $this->handle($request, 'user');
    }

    protected function getConfigPath(): string
    {
        return 'microservices.auth';
    }
}
