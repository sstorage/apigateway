<?php

namespace App\Http\Controllers;

use App\Http\Requests\GatewayRequest;

class UploaderController extends GatewayBaseController
{

    public function store(GatewayRequest $request)
    {
        return $this->handle($request, 'files');
    }

    public function delete(GatewayRequest $request, ...$params)
    {
        return $this->handle($request, 'file', $params);
    }

    protected function getConfigPath(): string
    {
        return 'microservices.uploader';
    }
}
