<?php

namespace App\Http\Controllers;

use App\Http\Requests\GatewayRequest;
use App\Services\MicroserviceClient;
use Illuminate\Routing\Controller as BaseController;

abstract class GatewayBaseController extends BaseController
{
    protected MicroserviceClient $client;

    public function __construct(MicroserviceClient $client)
    {
        $this->client = $client;
    }

    public function handle(GatewayRequest $request, string $urlAlias, array $urlParams = [])
    {
        $url = $this->client->getUrl($this->getConfigPath(), $urlAlias, $urlParams);
        return $this->client->gatewayRequest($request, $url);
    }

    abstract protected function getConfigPath(): string;
}
