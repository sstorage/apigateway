<?php

namespace App\Http\Controllers;

use App\Http\Requests\GatewayRequest;

class StatsController extends GatewayBaseController
{

    public function files(GatewayRequest $request)
    {
        return $this->handle($request, 'files');
    }

    public function myFiles(GatewayRequest $request)
    {
        return $this->handle($request, 'myfiles');
    }

    public function downloads(GatewayRequest $request, ...$params)
    {
        return $this->handle($request, 'downloads', $params);
    }

    protected function getConfigPath(): string
    {
        return 'microservices.stats';
    }
}
