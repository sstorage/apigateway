<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\DownloaderController;
use App\Http\Controllers\StatsController;
use App\Http\Controllers\UploaderController;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::group(['prefix' => 'v1'], function () {
    //Auth
    Route::group(['prefix' => 'auth'], function () {
        Route::post('/login', [AuthController::class, 'login']);
        Route::post('/registration', [AuthController::class, 'registration']);
        Route::group(['middleware' => ['verify-jwt-token']], function () {
            Route::get('/user', [AuthController::class, 'userInfo']);
            Route::post('/user', [AuthController::class, 'update']);
        });
    });

    //Files
    Route::group(['middleware' => ['verify-jwt-token']], function () {
        Route::delete('/files/{uid}', [UploaderController::class, 'delete']);
        Route::post('/files', [UploaderController::class, 'store']);
        Route::get('/files/{uid}', [DownloaderController::class, 'getFile']);
        Route::get('/files/{uid}/downloads', [StatsController::class, 'downloads']);
        Route::get('/myfiles', [StatsController::class, 'myFiles']);
    });

    //Statistics
    Route::get('/files', [StatsController::class, 'files']);
});
