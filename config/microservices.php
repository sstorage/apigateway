<?php

return [

    'timeout' => env('HTTP_CONNECTION_TIMEOUT', 30),

    //Auth microservice routes
    'auth' => [
        'base' => env('AUTH_API_URL', ''),
        'login' => env('AUTH_API_URL') . '/auth/login',
        'registration' => env('AUTH_API_URL') . '/auth/registration',
        'logout' => env('AUTH_API_URL') . '/auth/logout',
        'refresh' => env('AUTH_API_URL') . '/auth/refresh',
        'user' => env('AUTH_API_URL') . '/auth/user',
        'verify' => env('AUTH_API_URL') . '/auth/verify',
    ],

    //Downloader microservice routes
    'downloader' => [
        'base' => env('DOWNLOADER_API_URL', ''),
        'files' => env('DOWNLOADER_API_URL') . '/files',
        'file' => env('DOWNLOADER_API_URL') . '/files/%s',
    ],

    //Uploader microservice routes
    'uploader' => [
        'base' => env('UPLOADER_API_URL', ''),
        'files' => env('UPLOADER_API_URL') . '/files',
        'file' => env('UPLOADER_API_URL') . '/files/%s',
    ],

    'stats' => [
        'base' => env('STATS_API_URL', ''),
        'files' => env('STATS_API_URL') . '/files',
        'myfiles' => env('STATS_API_URL') . '/myfiles',
        'downloads' => env('STATS_API_URL') . '/files/%s/downloads',
    ]
];
